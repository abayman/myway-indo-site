<?php
use GuzzleHttp\Client;

if (! function_exists('API')) {
    function API($uri, $param = []) {
        $client = new GuzzleHttp\Client([
            'base_uri' => 'https://mustseeum.com/myway_dev/api/v1/',
            'verify' => false,
        ]);
        $res = $client->request('GET', $uri);
        return json_decode($res->getBody());
    }
}

if (! function_exists('API2')) {
    function API2($uri, $method='GET', $param = []) {
        $client = new GuzzleHttp\Client([
            'base_uri' => env('API_URL'),
            'verify' => false,
        ]);
        $res = $client->request($method, $uri,$param);
        return json_decode($res->getBody());
    }
}

if (! function_exists('WA')) {
    function WA()
    {
        //spasi ganti %20, : ganti %3A%0A
        return 'https://wa.me/6285764987673?text=No.%20Sponsor%3A%0ANama%20Sponsor%3A%0ANama%20New%20Member%3A%0ATempat%20tgl%20lahir%3A%0ANo.%20KTP/SIM%3A%0ANPWP%20(if%20any)%3A%0AAlamat%20lengkap%3A%0AEmail%20(if%20any)%3A%0ANo.%20Account%20Bank%3A%0ANama%20Pemilik%20Account%3A%0ANama%20Bank%3A%0ACabang%3A%0A';
    }
}

if (! function_exists('MY_PRICE')) {
    function MY_PRICE($number)
    {
        //spasi ganti %20, : ganti %3A%0A
        return 'Rp. '.number_format($number,0,'','.');
    }
}

