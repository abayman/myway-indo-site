<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function byCate(Request $request, $cate)
    {
        if ($request->get('sort')){
            $product = API2('product/list-cate/'.$cate.'?sort='.$request->get('sort'));
        }else{
            $product = API2('product/list-cate/'.$cate);
        }
        $category = API2('product/cate');


        return view('product.list', [
            'list' => $product->data,
            'slug' => $cate,
            'cate' => $category->data,
            'count' => count($product->data),
            'last_page' => $product->last_page,
            'page' => 2
        ]);
    }

    public function byCatePage($cate,$page)
    {
        $product = API2('product/list-cate/'.$cate.'?page='.$page);
        return response()->json([
            'data' => $product->data,
            'cate' => $cate,
            'page' => $page+1,
            'last_page' => $product->last_page
        ]);
    }

    public function list(Request $request)
    {
        $product = API2('product/get');
        $category = API2('product/cate');


        return view('product.list', [
            'list' => $product->data,
            'slug' => 'all',
            'cate' => $category->data,
            'count' => count($product->data)
        ]);
    }

    public function detail($cate,$slug,$id)
    {
        $api = API2('product/detail/'.$id);
        $other = API2('product/other/'.$id);

        return view('product.detail',[
            'data' => $api->data,
            'others' => $other->data,
        ]);
    }

    public function review(Request $request,$cate,$slug,$id)
    {
        $api = API2('product/detail/'.$id);
        return view('product.review',[
            'data' => $api->data,
        ]);
    }

    public function ReviewAdd(Request $request,$cate,$slug,$id)
    {
        $api = API2('product/detail/'.$id);
        return view('product.reviewAdd',[
            'data' => $api->data,
        ]);
    }

    public function ReviewPost(Request $request)
    {
        $input = $request->input();
        $post = [
            'form_params' => $input
        ];
        $post = API2('review/add','POST',$post);

        return redirect(route('product.review',['id' => $input['product_id'],'slug' => $input['slug'], 'cate' =>  $input['cate']]))
            ->with('status', 'Message Sent!');

    }

    public function search(Request $request)
    {
        $product = API2('product/search?q='.$request->q);
        $category = API2('product/cate');

        return view('product.list', [
            'list' => $product->data,
            'slug' => $request->q,
            'cate' => $category->data,
            'count' => count($product->data)
        ]);
    }
}
