<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BusinessController extends Controller
{
    public function index()
    {
        $api = API2('benefit/get');
        $wa = API2('benefit/wa');

        return view('business.index',['data'=> $api->data,'wa' => $wa]);
    }
}
