<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PromoController extends Controller
{
    public function index(Request $request,$page=1)
    {
        $api = API2('promo/get?page='.$page);
        return view('promo.index',[
            'data' => $api->data,
            'page' => $page+1
        ]);
    }

    public function detail($slug,$id)
    {
        $data = API2('promo/detail/'.$id);
        return view('promo.detail',['data' => $data->data]);
    }
}
