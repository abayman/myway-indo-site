<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StoriesController extends Controller
{
    public function index()
    {
        $api = API2('stories/get');
        return view('stories.index',['stories'=> $api->data]);
    }
    public function detail($slug,$id)
    {
        $id_arr = explode('-',$id);
        $id = end($id_arr);
        $api = API2('stories/detail/'.$id);
        $related = API2('stories/except/'.$id.'?limit=3');
        return view('stories.detail', [
            'data' => $api->data,
            'related' => $related->data
        ]);
    }
}
