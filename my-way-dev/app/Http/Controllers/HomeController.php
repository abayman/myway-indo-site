<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $data=[];

        $newArrival = API2('product/get?type=2');
        $rekomen = API2('product/get?type=1');
        $lead = API2('testi/get');
        $stories = API2('stories/get');
        $banner = API2('banner');

        return view('home.index',[
            'stories'=> $stories->data,
            'new_arrival' => $newArrival->data,
            'rekomen' => $rekomen->data,
            'lead' => $lead->data,
            'banner' => $banner->data
        ]);
    }
}
