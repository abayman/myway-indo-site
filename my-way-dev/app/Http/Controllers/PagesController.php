<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PagesController extends Controller
{
    public function about()
    {
        $api = API2('page/about');
        return view('pages.about',['data' => $api->data]);
    }

    public function contact()
    {
        $api = API2('page/contact');
        return view('pages.contact',['data' => $api->data]);
    }

    public function contactPost(Request $request)
    {
        $input = $request->input();
        $data = [
            'data' => $input['message'],
            'replay' => $input['email'],
            'name' => $input['firstName'].' '. $input['lastName'],
            'to' =>  env('MAIL_FROM_ADDRESS')
        ];

        Mail::send('mail', $data, function($message) use ($data)
        {
            $message->to($data['to'],  $data['name'])
                ->replyTo($data['replay'], $data['name'])
                ->subject('Contact MyWay');
        });

        return redirect(route('page.contact'))->with('status', 'Message Sent!');
    }
}
