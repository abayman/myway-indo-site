<?php


namespace App\Http\View\Composers;


use Illuminate\View\View;

class FooterComposer
{
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $menu = API2('page/footer');
        $view->with('footer', $menu->data);
    }
}
