<?php
namespace App\Http\View\Composers;

use Illuminate\View\View;

class MenuComposer
{

    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $menu = API2('page/nav');
        $view->with('menu', $menu->data);
    }
}


