<?php


Route::get('/', 'HomeController@index')->name('home');
Route::get('/promo', 'PromoController@index')->name('promo');
Route::get('/promo/{page}', 'PromoController@index')->name('promo.page');
Route::get('/promo/{slug}/{id}', 'PromoController@detail')->name('promo.detail');
Route::get('/business', 'BusinessController@index')->name('business');
Route::get('/stories', 'StoriesController@index')->name('stories');
Route::get('/stories/detail/{slug}-{id}', 'StoriesController@detail')->name('stories.detail');
Route::get('/about', 'PagesController@about')->name('page.about');
Route::get('/contact', 'PagesController@contact')->name('page.contact');
Route::post('/contact', 'PagesController@contactPost')->name('page.contact.post');
Route::get('/product/list', 'ProductController@list')->name('product.list');
Route::get('/category/{cate}', 'ProductController@byCate')->name('product.list');
Route::get('/category/{cate}/{page}', 'ProductController@byCatePage')->name('product.list.page');
Route::get('/product/search', 'ProductController@search')->name('product.search');
Route::get('/product/{cate}/{slug}/{id}', 'ProductController@detail')->name('product.detail');
Route::get('/product/{cate}/{slug}/{id}/review', 'ProductController@review')->name('product.review');
Route::get('/product/{cate}/{slug}/{id}/review/add', 'ProductController@ReviewAdd')->name('product.review.add');
Route::post('/product/review/post', 'ProductController@ReviewPost')->name('product.review.post');
