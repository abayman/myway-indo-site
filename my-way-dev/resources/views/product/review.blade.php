@extends('master')
@section('title', 'Home')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".owl-carousel").owlCarousel({
                items:5,
                nav:true,
                navText: ["<i class=\"fas fa-chevron-left\"></i>","<i class=\"fas fa-chevron-right\"></i>"]

            });
        });
    </script>
@stop

@section('content')
    <div class="container-stories-detail">
        <div class="row  mb90">
            <div class="col-12 nopd mb50">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb f20">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">category</a></li>
                        <li class="breadcrumb-item text-lowercase">{{$data->cate_child->name}}</li>
                        <li class="breadcrumb-item text-lowercase">{{$data->name}}</li>
                        <li class="breadcrumb-item text-lowercase active" aria-current="page">review</li>
                    </ol>
                </nav>
            </div>
            <div class="col-lg-12 col-md12">
                <div class="row">
                    <div class="col-lg-12 col-md12 mb20">
                        <span class="left f25 bold">Member Reviews</span>
                        <span class="right f20 unggu">
                            <a href="{{route('product.review.add',['id' => $data->id,'slug' => $data->slug, 'cate' =>  $data->category->slug ])}}" class="unggu bold">
                                <i class="fas fa-plus"></i> Write a Review
                            </a>
                        </span>
                    </div>
                    <div class="col-lg-10 col-md-10 offset-1 border-abu" style="padding: 20px 30px;">
                        <h1 class="f25 black text-center">Overall Rating</h1>
                        <div class="row">
                            <div class="col-md-4 col-4 br-pink">
                                <span class="black text-center d-block">
                                    <span class="f50 bold">{{$data->rating->rating}}</span>
                                    <span class="f30">/ 5.0</span>
                                </span>
                                <span class="text-center d-block start f25">
                                    @for($i=1;$i<=5;$i++)
                                        @if(round($data->rating->rating) >= $i)
                                            <i class="fas fa-star"></i>
                                        @else
                                            <i class="far fa-star"></i>
                                        @endif
                                    @endfor
                                </span>
                                <span class="f20 text-center d-block">{{$data->rating->total_review}} Reviews</span>
                            </div>
                            <div class="col-md-8 col-8">
                                <div class="row">
                                    @for ($i = 5; $i > 0; $i--)
                                        <div class="col-md-2" style="max-width: 10%;">
                                            <span class="start d-block">
                                                <span class="black f20">{{$i}}</span> <i class="fas fa-star f25"></i>
                                            </span>
                                        </div>
                                        <div class="col-md-9 mt10">
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" style="width: {{$data->rating->detail->$i->nilai}}" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-1">
                                            <span>{{$data->rating->detail->$i->total}}</span>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row  mb90">
            <div class="col-md-12">
                <div class="left">
                    <span class="f25">Reviewed by {{$data->rating->total_review}} Members</span>
                </div>
                <div class="right">

                </div>
            </div>
            <hr class="unggu w-100">
            <div class="col-md-12">
                <div class="row">
                     @foreach($data->comments as $row)
                        <div class="col-md-12 mb50">
                            <div class="row">
                                <div class="col-md-1">
                                    <img src="{{$data->image[0]}}" class="d-block w-100" alt="{{$data->name}}">
                                </div>
                                <div class="col-md-10">
                                    <span class="d-block start f25">
                                        @for($i=1;$i<=5;$i++)
                                            @if(round($row->rating) >= $i)
                                                <i class="fas fa-star"></i>
                                            @else
                                                <i class="far fa-star"></i>
                                            @endif
                                        @endfor
                                    </span>
                                    <span class="d-block f16"><strong>By: </strong> {{$row->name}}</span>
                                    <span class="d-block f16"><strong>Submitted: </strong> {{date('d M Y', strtotime($row->created_at))}}</span>
                                </div>
                                <div class="col-md-12">
                                    <p class="f16">{{$row->comments}}</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@stop
