@extends('master')
@section('title', 'Home')


@section('content')
    <div class="banner about">
        <div class="desc">
            <h1 class="bold">PRODUCT</h1>
        </div>
        <img src="{{asset('assets/image/dummy/stories_banner.png')}}" alt="">
    </div>
    <div class="container-stories">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb f20">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">category</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$slug}}</li>
            </ol>
        </nav>
        <div class="row mt90">
            <div class="col-3">
                <div class="sidebar">
                    <h2 class="title f25 bold">CATEGORIES</h2>
                    <div class="accordion" id="accordion">
                        @foreach($cate as $row )
                        <div class="card">
                            <div class="card-header" id="{{$loop->index}}">
                                <h2 class="mb-0">
                                    <a class="btn btn-link f20 nopd"  href="{{'/category/'.$row->slug}}">
                                        {{$row->name}}
                                    </a>
                                    @if(count($row->children) > 0)
                                    <span class="right icon pointer" data-toggle="collapse" data-target="#collapse{{$loop->index}}" aria-expanded="false" aria-controls="collapse{{$loop->index}}">
                                        <i class="fas fa-plus"></i>
                                    </span>
                                    @endif
                                </h2>
                            </div>
                            @if(count($row->children) > 0)
                            <div id="collapse{{$loop->index}}" class="collapse" aria-labelledby="{{$loop->index}}" data-parent="#accordion">
                                <div class="card-body">
                                    <ul>
                                        @foreach($row->children as $sub_cate)
                                            @if(count($sub_cate->children) > 0)
                                                <li class="f18"><a href="{{'/category/'.$sub_cate->slug}}">{{$sub_cate->name}}</a></li>
                                                @foreach($sub_cate->children as $subto)
                                                    <li class="f18"><a href="{{'/category/'.$subto->slug}}">{{$subto->name}}</a></li>
                                                @endforeach
                                            @else
                                                <li class="f18"><a href="{{'/category/'.$sub_cate->slug}}">{{$sub_cate->name}}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            @endif
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-9">
                <div class="row mb90">
                    <div class="col-12">
                        <div class="left" style="margin-top: 1.5%;">
                            <span class="f18 bold">SHOWING <span class="count">{{$count}}</span> RESULTS</span>
                        </div>
                        <div class="right">

                            <div class="dropdown">
                                <span class="mr-3">Sorting: </span>
                                <button class="btn my-dropdown dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Lowest price
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="{{'/category/'.$slug.'?sort=1'}}">Lowest Price</a>
                                    <a class="dropdown-item" href="{{'/category/'.$slug.'?sort=2'}}">Highest Price</a>
                                    <a class="dropdown-item" href="{{'/category/'.$slug.'?sort=3'}}">Most Rated</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row append-data">
                    @if($count > 0)
                    @foreach($list as $row)
                    <div class="col-3">
                        <a href="{{route('product.detail',['id' => $row->id,'slug' => $row->slug, 'cate' =>  $row->category->slug ])}}">
                            <div class="box-produk">
                                <div class="top">
                                    <div class="right">
                                        <i class="far fa-heart f20 black"></i>
                                    </div>
                                </div>
                                <img src="{{$row->image[0]}}" class="d-block w-100" alt="{{$row->name}}">
                                <hr class="unggu-150">
                                <span class="f15 hitam text-center d-block f-light">{{$row->code}}</span>
                                <h1 class="f16">{{$row->name}}</h1>
                                <span class="text-center d-block start f16">
                                    @for($i=1;$i<=5;$i++)
                                        @if(round($row->rating->rating) >= $i)
                                            <i class="fas fa-star"></i>
                                        @else
                                            <i class="far fa-star"></i>
                                        @endif
                                    @endfor
                                </span>
                                <span class="d-block text-center">
                                    @if ($row->price_discount > 0)
                                        <span class="mr-1 f18 text-line-through">{{MY_PRICE($row->price)}}</span>
                                        <span class="ml-1 bold f18 unggu">{{MY_PRICE($row->price_discount)}}</span>
                                    @else
                                        <span class="mr-1 bold f18">{{MY_PRICE($row->price)}}</span>
                                    @endif
                                </span>
                            </div>
                        </a>
                    </div>
                    @endforeach
                    @else
                        <div class="col-12">
                            <p class="text-center">Product Not found</p>
                        </div>
                    @endif
                </div>
                @if($last_page != 1)
                <div class="load-more product mt90">
                    <hr class="pink left">
                    <a id="load-more" page="{{$page}}" slug="{{$slug}}" class="btn btn-lg" role="button" aria-pressed="true">Load More</a>
                    <hr class="pink right">
                </div>
                @endif
            </div>
        </div>
    </div>

@stop

@section('script')
    <script>

        $('#load-more').click(function () {
            console.log({{$page}});
            var slug = $(this).attr('slug');
            var page = $(this).attr('page');
            var count = $('.count').text();

            $.ajax({
                url: '/category/'+slug+'/'+page,
                dataType: 'json',
                success: function (resp) {
                    var total = parseInt(count) + resp.data.length;
                    $('#load-more').attr('page',resp.page);
                    $('.count').text(total);
                    if (page == resp.last_page){
                        $('.load-more').hide();
                    }
                    var c ='';

                    $.each(resp.data, function( index, val ) {
                        var url = '/product/'+val.category.slug+'/'+val.slug+'/'+val.id;
                        c +='<div class="col-3">';
                        c +='<a href="'+url+'">';
                        c +='<div class="box-produk">';
                        c +='<div class="top">';
                        c +='<div class="right">';
                        c +='<i class="far fa-heart f20 black"></i>';
                        c +='</div>';
                        c +='</div>';
                        c +='<img src="'+val.image[0]+'" class="d-block w-100" alt="'+val.name+'">';
                        c +='<hr class="unggu-150">';
                        c +='<span class="f15 hitam text-center d-block f-light">'+val.code+'</span>';
                        c +='<h1 class="f16">'+val.name+'</h1>';
                        c +='<span class="text-center d-block start f16">';
                        for (var i = 1;i<=5;i++){
                            if (Math.round(val.rating.rating) >= i){
                                c +='<i class="fas fa-star"></i>';
                            }else{
                                c +='<i class="far fa-star"></i>';
                            }
                        }
                        c +='</span>';
                        c +='<span class="d-block text-center">';
                        if (val.price_discount > 0){
                            c +='<span class="mr-1 f18 text-line-through">'+formatMyMoney(val.price)+'</span>';
                            c +='<span class="ml-1 bold f18 unggu">'+formatMyMoney(val.price_discount)+'</span>';
                        }else{
                            c +='<span class="mr-1 bold f18">'+formatMyMoney(val.price)+'</span>';
                        }
                        c +='</span>';
                        c +='</div>';
                        c +='</a>';
                        c +='</div>';
                        console.log(url)
                    });
                    $('.append-data').append(c);
                },
            });
        });
        function formatMyMoney(price) {
            var formattedOutput = new Intl.NumberFormat('Rp', {
                style: 'currency',
                currency: 'IDR',
                minimumFractionDigits: 0,
            });
            return formattedOutput.format(price).replace('IDR', 'Rp. ').replace(',','.')
        }
    </script>

@endsection
