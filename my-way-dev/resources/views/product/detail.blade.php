@extends('master')
@section('title', 'Home')

@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".owl-carousel").owlCarousel({
                items:5,
                nav:true,
                navText: ["<i class=\"fas fa-chevron-left\"></i>","<i class=\"fas fa-chevron-right\"></i>"]

            });
        });
    </script>
@stop

@section('content')
    <div class="container-stories-detail">
        <div class="row  mb90">
            <div class="col-12 nopd mb50">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb f20">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">category</a></li>
                        <li class="breadcrumb-item text-lowercase"><a href="{{route('product.list',['cate' => $data->category->slug])}}">{{$data->cate_child->name}}</a></li>
                        <li class="breadcrumb-item text-lowercase active" aria-current="page">{{$data->name}}</li>
                    </ol>
                </nav>
            </div>
            <div class="col-12 p90 border-abu">
                <div class="row">
                    <div class="col-4">
                        <img src="{{$data->image[0]}}" alt="">
                        <div class="w-100 left mb20">
                            <div class="social-media">
                                <span class="f20 bold mr15">Share</span>
                                <span class="img">
                                    <img style="width: 40px;" src="{{asset('assets/image/icon/ic_facebook.png')}}" alt="">
                                </span>
                                <span class="img">
                                    <img style="width: 40px;" src="{{asset('assets/image/icon/ic_instagram.png')}}" alt="">
                                </span>
                                <span class="img">
                                    <img style="width: 40px;" src="{{asset('assets/image/icon/ic_twitter.png')}}" alt="">
                                </span>
                                <span class="img">
                                    <img style="width: 40px;" src="{{asset('assets/image/icon/ic_yt.png')}}" alt="">
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <h1 class="f35 pink">{{$data->name}}</h1>
                        <div class="w-100 left mb20">
                            <span class="d-block">
                                @if ($data->price_discount > 0)
                                    <span class="ml-1 f30 text-line-through">{{MY_PRICE($data->price)}}</span>
                                    <span class="mr-1 bold f30 unggu">{{MY_PRICE($data->price_discount)}}</span>
                                @else
                                    <span class="mr-1 bold f30">{{MY_PRICE($data->price)}}</span>
                                @endif
                            </span>
                            <span class="mt20 f16">Code : {{$data->code}}</span>
                        </div>
                        <div class="w-100 left mb20">
                            @if($data->rating->total_review > 0)
                                <span class="start f18 mr-5">
                                    @for($i=1;$i<=5;$i++)
                                        @if(round($data->rating->rating) >= $i)
                                            <i class="fas fa-star"></i>
                                        @else
                                            <i class="far fa-star"></i>
                                        @endif
                                    @endfor
                                </span>
                                <span> <a href="{{route('product.review',['id' => $data->id,'slug' => $data->slug, 'cate' =>  $data->cate_child->name ])}}" class="pink a-underline">{{$data->rating->total_review}} REVIEWS</a></span>
                            @else
                                <span>
                                     <a href="{{route('product.review.add',['id' => $data->id,'slug' => $data->slug, 'cate' =>  $data->cate_child->name ])}}" class="pink bold">
                                        <i class="fas fa-plus"></i> Write a Review
                                    </a>
                                </span>
                            @endif

                        </div>
                        <div class="w-100 left mb20">
                            <h2 class="f25 bold mb20">Description</h2>
                            {!! $data->description !!}
                        </div>
                        <div class="w-100 left mb20">
                            <h2 class="f25 bold mb20">Benefit</h2>
                            {!! $data->benefit !!}
                        </div>
                        <div class="w-100 left mb20">
                            <h2 class="f25 bold mb20">Ingredients</h2>
                            {!! $data->ingredients !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <h2 class="f35 bold text-center">YOU MIGHT ALSO LIKE</h2>
                <div class="owl-carousel owl-theme my-carousel" style="padding: 0 80px;">
                    @foreach ($others as $other)
                        <div class="item">
                            <a href="{{route('product.detail',['id' => $other->id,'slug' => $other->slug, 'cate' =>  $other->category->slug ])}}">
                                <div class="box-produk p-2">
                                    <div class="top">
                                        <div class="right">
                                            <i class="far fa-heart f20 black"></i>
                                        </div>
                                    </div>
                                    <img src="{{$other->image[0]}}" class="d-block w-100" alt="lorem">
                                    <hr class="unggu-150">
                                    <span class="f15 hitam text-center d-block f-light">{{$other->code}}</span>
                                    <h1 class="black">{{$other->name}}</h1>
                                    <span class="text-center d-block start f16 mb20">
                                        @for($i=1;$i<=5;$i++)
                                            @if(round($other->rating->rating) >= $i)
                                                <i class="fas fa-star"></i>
                                            @else
                                                <i class="far fa-star"></i>
                                            @endif
                                        @endfor
                                    </span>
                                    <span class="d-block text-center">
                                        @if ($other->price_discount > 0)
                                            <span class="ml-1 f20 text-line-through">{{MY_PRICE($other->price)}}</span>
                                            <span class="mr-1 bold f20 unggu">{{MY_PRICE($other->price_discount)}}</span>
                                        @else
                                            <span class="mr-1 bold f20">{{MY_PRICE($other->price)}}</span>
                                        @endif
                                    </span>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@stop
