@extends('master')
@section('title', 'Home')

@section('css')
    <link rel="stylesheet" href="{{asset('assets/js/bar-rating/themes/css-stars.css')}}"/>
@stop
@section('script')
    <script src="{{asset('assets/js/bar-rating/jquery.barrating.min.js')}}"></script>
    <script type="text/javascript">
        $(function() {
            $('#my-rating').barrating({
                theme: 'css-stars'
            });
        });
    </script>

@stop

@section('content')
    <div class="container-stories-detail">
        <div class="row  mb90">
            <div class="col-md-12 nopd mb50">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb f20">
                        <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">category</a></li>
                        <li class="breadcrumb-item text-lowercase">{{$data->cate_child->name}}</li>
                        <li class="breadcrumb-item text-lowercase">{{$data->name}}</li>
                        <li class="breadcrumb-item text-lowercase active" aria-current="page">Add review</li>
                    </ol>
                </nav>
            </div>
            <div class="col-md-12">
                <h1 class="f25 bold black mb20">Write a Review</h1>
                <p>Thank you for sharing your thoughts about one of our products! Please focus on the product’s performance and quality, and don’t mention prices or competitors. You may also leave service comments below. Please note that product reviews that do not comply with our review guidelines or the Terms and Conditions of Use for this site may be removed. Also, note that reviews posted here may be used by us in our other channels, such as on our social media pages and in our brochures.</p>
                <div class="row">
                    <div class="col-md-2">
                        <img src="{{$data->image[0]}}" class="d-block w-100" alt="...">
                    </div>
                    <div class="col-md-10" style="margin: auto;">
                        <h1 class="bold f35 unggu">{{$data->name}}</h1>
                        <span class="d-block f20">Code: <strong>{{$data->code}}</strong></span>
                    </div>
                </div>
            </div>
            <div  class="col-md-12">
                <span class="mb20 d-block">*Required Fields</span>
                <form action="{{route('product.review.post')}}" method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="product_id" value="{{$data->id}}">
                    <input type="hidden" name="cate" value="{{$data->cate_child->name}}">
                    <input type="hidden" name="slug" value="{{$data->slug}}">
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="bold">Your Rating*</label>
                        <select id="my-rating" name="rating">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1" class="bold">Your Name*</label>
                        <input type="text" name="name" required="required" class="form-control myway" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Your fully name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1"  class="bold">Comment*</label>
                        <textarea name="comments" required="required" class="form-control  myway" id="exampleFormControlTextarea1" rows="7" placeholder="What’s your opinion about this product? Tell us the great things about it."></textarea>
                    </div>
                    <button type="submit" class="btn btn-unggu btn-lg">SEND REVIEW</button>
                </form>
            </div>
        </div>

    </div>

@stop
