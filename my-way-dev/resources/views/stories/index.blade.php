@extends('master')
@section('title', 'Home')


@section('content')
    <div class="banner about">
        <div class="desc">
            <h1 class="bold">STORIES</h1>
        </div>
        <img src="{{asset('assets/image/dummy/stories_banner.png')}}" alt="">
    </div>
    <div class="container-stories">
        <div class="row">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                @foreach ($stories as $key =>$val)
                    <li class="nav-item">
                        <a class="nav-link {{($loop->index == 0) ? 'active' : ''}}" id="{{$key}}-tab" data-toggle="tab" href="#{!! $key !!}" role="tab" aria-controls="home" aria-selected="true">{{$key}}</a>
                    </li>
                @endforeach
            </ul>
            <div class="tab-content" id="myTabContent">
                @foreach ($stories as $key => $val)
                    <div class="tab-pane fade show {{($loop->index == 0) ? 'active' : ''}}" id="{!! $key !!}" role="tabpanel" aria-labelledby="{!! $key !!}-tab">
                        <div class="row">
                            @php
                            $count=0;
                            @endphp
                            @foreach ($val as $row)
                                <div class="col-6 mb200 box-promo">
                                    <a href="{{route('stories.detail',['id' => $row->id, 'slug' => $row->slug])}}">
                                        <img src="{{$row->image_url}}" alt="{{$row->title}}">
                                        <div class="desc">
                                            <div class="top">
                                                <span class="left">BY: {{$row->author}}</span>
                                                <span class="right">{{date('d M, Y', strtotime($row->created_at))}}</span>
                                            </div>
                                            <h1>{{$row->title}}</h1>
                                            <p class="ellipsis">{!! $row->short_description !!}</p>
                                        </div>
                                    </a>
                                </div>
                                @php
                                    $count++;
                                @endphp
                            @endforeach
                        </div>
                        @if ($count >=4)
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="load-more">
                                        <hr class="pink left">
                                        <a href="#" class="btn btn-lg" role="button" aria-pressed="true">SHOP NOW</a>
                                        <hr class="pink right">
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>

    </div>

@stop
