@extends('master')
@section('title', 'Home')


@section('content')
    <div class="container-stories-detail">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb f20">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">Stories</a></li>
                <li class="breadcrumb-item">Blog</li>
                <li class="breadcrumb-item text-lowercase active" aria-current="page">{{$data->title}}</li>
            </ol>
        </nav>
        <h1 class="f55 mt50 mb30">{{$data->title}}</h1>
        <div class="top">
            <span class="left">
                <span class="author">By: {{$data->author}} </span>
                <hr class="pink mt20 mr15">
                <span class="date">{{date('d F Y',strtotime($data->created_at))}}</span>
            </span>
{{--            <span class="right cate">Beauty Tips</span>--}}

        </div>
        <div class="box">
            <p class="f25 hitam">
                {!! $data->long_description !!}
            </p>
        </div>
        <hr class="unggu mt20">
{{--        <div class="row nomr">--}}
{{--            <div class="tags black f25 ">--}}
{{--                <span class="mr60">Beauty</span>--}}
{{--                <span class="mr60">Tips</span>--}}
{{--                <span class="mr60">make up</span>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="row nomr mt90 mb90">
            <div class="related-articles w-100">
                <span class="f30 hitam mr-5">RELATED ARTICLES</span>
                <div class="article">
                    <div class="row">
                        @foreach ($related as $other)
                            <div class="col-4">
                                <a href="{{route('stories.detail',['id' => $other->id, 'slug' => $other->slug])}}">
                                    <img src="{{$other->image_url}}" alt="">
                                    <div class="desc f16">
                                        {{$other->title}}
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

        </div>
    </div>

@stop
