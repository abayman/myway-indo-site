@extends('master')
@section('title', 'Home')
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.min.css" />
@stop
@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
    <script>
        $(document).ready(function(){
            $(".owl-product").owlCarousel({
                items:5,
                nav:true,
                navText: ["<i class=\"fas fa-chevron-left\"></i>","<i class=\"fas fa-chevron-right\"></i>"],
                responsiveClass:true,
                responsive:{
                    0:{
                        items:5,
                        nav:true
                    },
                    360:{
                        items:2,
                        nav:true
                    },
                    600:{
                        items:2,
                        nav:true
                    },
                    1366:{
                        items:5,
                        nav:true
                    },
                    1920:{
                        items:5,
                        nav:true
                    },
                }

            });
            $(".owl-banner").owlCarousel({
                items:1,
                nav:false,
                navText: ["<i class=\"fas fa-chevron-left\"></i>","<i class=\"fas fa-chevron-right\"></i>"],
                responsiveClass:true,
            });
        });
    </script>
@stop

@section('content')
    <div class="banner home">
        <div class="owl-carousel owl-banner owl-theme" style="padding: 1px;">
            @foreach($banner as $row)
            <div class="item">
                <img class="d-none d-sm-block" src="{{$row->image_url}}" alt="">
                <img class="d-sm-block d-md-none w-100"  src="{{$row->image_mobile_url}}" alt="">
            </div>
            @endforeach
        </div>
    </div>
    <div class="new-arrivals">
        <div class="container-produk">
            <h2>New Arrivals</h2>
            <div class="owl-carousel owl-product owl-theme my-carousel">
                @foreach($new_arrival as $row)
                    <div class="item">
                        <a href="{{route('product.detail',['id' => $row->id,'slug' => $row->slug, 'cate' =>  $row->category->slug ])}}">
                            <div class="box-produk p-2">
                                <div class="top">
                                    <div class="left">
                                        <img src="{{asset('./assets/image/icon/new.svg')}}" alt="">
                                    </div>
                                    <div class="right">
                                        <i class="far fa-heart f20 black"></i>
                                    </div>
                                </div>
                                <img src="{{$row->image[0]}}" class="d-block w-100" alt="{{$row->name}}">
                                <hr class="unggu-150">
                                <span class="f15 hitam text-center d-block f-light">{{$row->code}}</span>
                                <h1 class="ellipsis">{{$row->name}}</h1>
                                <span class="text-center d-block start f25">
                                    @for($i=1;$i<=5;$i++)
                                        @if(round($row->rating->rating) >= $i)
                                            <i class="fas fa-star"></i>
                                        @else
                                            <i class="far fa-star"></i>
                                        @endif
                                    @endfor
                                </span>
                                <span class="d-block text-center">
                                     @if ($row->price_discount > 0)
                                        <span class="ml-1 f20 text-line-through">{{MY_PRICE($row->price)}}</span>
                                        <span class="mr-1 bold f20 unggu">{{MY_PRICE($row->price_discount)}}</span>
                                    @else
                                        <span class="mr-1 bold f20">{{MY_PRICE($row->price)}}</span>
                                    @endif
                                </span>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="new-arrivals">
        <div class="container-produk">
            <h2>Recommended For You</h2>
            <div class="owl-carousel owl-product owl-theme my-carousel">
                @foreach($rekomen as $row)
                    <div class="item">
                        <a href="{{route('product.detail',['id' => $row->id,'slug' => $row->slug, 'cate' =>  $row->category->slug ])}}">
                            <div class="box-produk p-2">
                                <div class="top">
                                    <div class="right">
                                        <i class="far fa-heart f20 black"></i>
                                    </div>
                                </div>
                                <img src="{{$row->image[0]}}" class="d-block w-100" alt="{{$row->name}}">
                                <hr class="unggu-150">
                                <span class="f15 hitam text-center d-block f-light">{{$row->code}}</span>
                                <h1 class="ellipsis">{{$row->name}}</h1>
                                <span class="text-center d-block start f25">
                                    @for($i=1;$i<=5;$i++)
                                        @if(round($row->rating->rating) >= $i)
                                            <i class="fas fa-star"></i>
                                        @else
                                            <i class="far fa-star"></i>
                                        @endif
                                    @endfor
                                </span>
                                <span class="d-block text-center">
                                    @if ($row->price_discount > 0)
                                        <span class="ml-1 f20 text-line-through">{{MY_PRICE($row->price)}}</span>
                                        <span class="mr-1 bold f20 unggu">{{MY_PRICE($row->price_discount)}}</span>
                                    @else
                                        <span class="mr-1 bold f20">{{MY_PRICE($row->price)}}</span>
                                    @endif
                                </span>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="join">
        <div>
            <div class="desc">
                <h1 class="bold">MAKE IT HAPPEN</h1>
                <h3 class="mb40 mt20">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse massa felis, aliquam sed consequat ac, dictum eget lacus.</h3>
                <a href="{{route('business')}}" class="btn btn-abu btn-lg" role="button" aria-pressed="true">JOIN US NOW</a>
            </div>
            <img class="d-none d-sm-block" src="{{asset('assets/image/dummy/banner_join.png')}}" alt="">
            <img class="d-sm-block d-md-none w-100"  src="{{asset('assets/image/dummy/join-m.png')}}" alt="">
        </div>
    </div>
    <div class="stories">
        <div class="row">
            <h1>STORIES FROM LEADERS</h1>
            @foreach ($lead as $row)
                <div class="col-sm-12 col-md-4">
                    <img src="{{$row->image_url}}" alt="{!! $row->short_description !!}">
                    <span class="comment ellipsis">{!! $row->short_description !!}</span>
                    <span class="name">{{$row->name.', '.$row->city}}</span>
                </div>
            @endforeach
        </div>
    </div>
    <hr class="unggu">
    <div class="our-stories">
        <div class="row">
            <h1>Our Stories</h1>
            <div class="w-100">
                <ul class="nav  my-tabs category" id="myTab" role="tablist">
                    @foreach ($stories as $key =>$val)
                        <li class="nav-item">
                            <a class="nav-link {{($loop->index == 0) ? 'active' : ''}}" id="{{$key}}-tab" data-toggle="tab" href="#{!! $key !!}" role="tab" aria-controls="home" aria-selected="true">{{$key}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                @foreach ($stories as $key => $val)
                <div class="tab-pane fade show {{($loop->index == 0) ? 'active' : ''}}" id="{!! $key !!}" role="tabpanel" aria-labelledby="{!! $key !!}-tab">
                    <div class="row">
                        @foreach ($val as $row)
                            @if ($loop->index < 3)
                                <div class="col-sm-12 col-md-4">
                                    <a href="{{route('stories.detail',['id' => $row->id, 'slug' => $row->slug])}}">
                                        <img src="{{$row->image_url}}" alt="{{$row->title}}">
                                        <span class="title d-block text-center">{{$row->title}}</span>
                                        <span class="desc d-block text-center ellipsis">{!! $row->short_description !!}}</span>
                                        <span class="read-more d-block text-center">
                                    <hr class="pink left">
                                    READ MORE
                                    <hr class="pink right">
                                </span>
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
@stop
