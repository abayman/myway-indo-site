<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    {{--FONT--}}
    <link rel="stylesheet" href="{{asset('assets/font/Lato/stylesheet.css')}}">
    <!-- CSS -->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/fontawesome/all.min.css')}}">
    @yield('css')
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}?v={{date('his')}}">
    <title>My-way - @yield('title')</title>
</head>
<body>
<div class="container">
    <div class="header">
        <div class="my-nav d-none d-sm-block">
            <div class="my-logo">
                <a href="{{route('home')}}">
                    <img src="{{asset('assets/image/logo.png')}}" alt="my-way">
                </a>
            </div>
            <div class="my-nav-items">
                <ul class="nav left">
                    @foreach ($menu as $row)
                        @if (count($row->children) > 0)
                            <li class="nav-item">
                            {{--Menu--}}
                                <a class="nav-link" href="{{$row->url}}">{{$row->title}}</a>
                                <div class="box-menu">
                                    <ul class="submenu">
                                        @foreach($row->children as $children)
                                            <li class="cate-nav">
                                                {{--Category--}}
                                                <a href="{{$children->url}}">{{$children->title}}</a>
                                                @if(isset($children->children))
                                                    <ul class="sub-sub">
                                                        @foreach($children->children as $child)
                                                            @if(count($child->children) > 0)
                                                                <div class="w-50">
                                                                    {{--Sub categoty--}}
                                                                    <h1>{{$child->title}}</h1>
                                                                    @foreach($child->children as $subto)
                                                                        {{--Child--}}
                                                                        <li><a href="{{$subto->url}}">{{$subto->title}}</a></li>
                                                                    @endforeach
                                                                </div>
                                                            @else
                                                                <li><a href="{{$child->url}}">{{$child->title}}</a></li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                    <div class="border-menu"></div>
                                </div>
                            </li>
                        @else
                            <li class="nav-item">
                                <a class="nav-link {{ ($loop->index == 0) ? 'active' : '' }}" href="{{$row->url}}">{{$row->title}}</a>
                            </li>
                        @endif
                    @endforeach
                </ul>
                <div class="my-search">
                    <form class="form-inline my-2 my-lg-0" action="{{route('product.search')}}" method="get">
                        <input class="form-control mr-sm-2" type="search" name="q" placeholder="Search" aria-label="Search">
                        <button class="my-2 my-sm-0 btn-search" type="submit" >
                            <img src="{{asset('assets/image/icon/search-480.png')}}" alt="my-way">
                        </button>
                    </form>
{{--                    <img src="{{asset('assets/image/icon/search-480.png')}}" alt="my-way">--}}
                </div>
            </div>

        </div>
        {{--        Nav Mobile--}}
        <nav class="navbar navbar-expand-lg navbar-light bg-light d-sm-block d-md-none">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="my-logo">
                <a href="{{route('home')}}">
                    <img src="{{asset('assets/image/logo-m.png')}}" alt="my-way">
                </a>
            </div>

            <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Disabled</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <div class="body">
        @yield('content')
    </div>

    <hr class="unggu">
    <div class="footer">
        <div class="row nomr">
            <div class="menu-left col-sm-12 col-lg-7">
                <div class="row">
                    @foreach ($footer->footer as $row)
                        <div class="col-md-4 col-sm-12">
                            <h2>{{$row->title}}</h2>
                            <ul>
                                @if (isset($row->children))
                                    @foreach ($row->children as $children)
                                        <li><a href="{{$children->url}}">{{$children->title}}</a></li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="social-media  col-sm-12 col-lg-4">
                <h2>BE SOCIAL</h2>
                <span class="img">
                    <a href="{{$footer->fb}}" target="_blank">
                        <img src="{{asset('assets/image/icon/ic_facebook.png')}}" alt="">
                    </a>
                </span>
                <span class="img">
                    <a href="{{$footer->instagram}}" target="_blank">
                        <img src="{{asset('assets/image/icon/ic_instagram.png')}}" alt="">
                    </a>
                </span>
                <span class="img">
                    <a href="{{$footer->twitter}}" target="_blank">
                        <img src="{{asset('assets/image/icon/ic_twitter.png')}}" alt="">
                    </a>
                </span>
                <span class="img">
                    <a href="{{$footer->youtube}}" target="_blank">
                       <img src="{{asset('assets/image/icon/ic_yt.png')}}" alt="">
                    </a>
                </span>
                <span class="mt-lg-5 d-block">
                © 2019 My Way. All rights reserved
            </span>
            </div>
        </div>
    </div>
    <hr class="ungguH49 mt20">
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{asset('assets/js/jquery-3.2.1.slim.min.js')}}"></script>
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
@yield('script')
<script src="{{asset('assets/js/custom.js')}}"></script>
</body>
</html>
