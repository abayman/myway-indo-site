@extends('master')
@section('title', 'Home')


@section('content')
    <div class="banner about">
        <div class="desc">
            <h1 class="bold">ABOUT US</h1>
        </div>
        <img src="{{asset('assets/image/dummy/about_banner.png')}}" alt="">
    </div>
    <div class="container-business">
        <div class="box about">
            <h1>MY WAY INDONESIA</h1>
            <div class="row">
                {!! $data[0]->about !!}
            </div>
        </div>
    </div>

@stop
