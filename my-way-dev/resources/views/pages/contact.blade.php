@extends('master')
@section('title', 'Home')


@section('content')
    <div class="banner about">
        <div class="desc">
            <h1 class="bold">CONTACT US</h1>
        </div>
        <img src="{{asset('assets/image/dummy/about_banner.png')}}" alt="">
    </div>
    <div class="container-business">
        <div class="why contact mb30">
            <h1 class="f50">Contact Information</h1>
        </div>
        <div class="box contact">
            @foreach($data as $row)
            <div class="col-6 mb50">
                <h2 class="f25">{{$row->title}}</h2>
                <span class="d-block mt20 f20">{{$row->address}}</span>
                <span class="d-block mt20 f20">Phone: {{$row->phone}}</span>
            </div>
            @endforeach
            <div class="col-12 mt20">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <form action="{{route('page.contact.post')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="form-row">
                            <div class="col pr-5">
                                <input type="text" name="firstName" required="required" class="form-control" placeholder="First name*">
                            </div>
                            <div class="col pl-5">
                                <input type="text" name="lastName" required="required" class="form-control" placeholder="Last name*">
                            </div>
                        </div>
                    </div>
                    <div class="form-group mt50">
                        <input type="text" name="email" required="required" class="form-control" id="formGroupExampleInput" placeholder="Email Address*">
                    </div>
                    <div class="form-group mt50">
                        <textarea class="form-control" required="required" name="message" id="exampleFormControlTextarea1" placeholder="What is your question?" rows="7"></textarea>
                    </div>
                    <div class="interest-join">
                        <button type="submit" class="btn btn-unggu btn-lg" role="button" aria-pressed="true">SEND QUESTION</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@stop
