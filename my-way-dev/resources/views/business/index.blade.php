@extends('master')
@section('title', 'Home')


@section('content')
    <div class="banner about">
        <div class="desc">
            <h1 class="bold f70">BUSINESS OPPORTUNITY</h1>
        </div>
        <img src="{{asset('assets/image/dummy/business.png')}}" alt="">
    </div>
    <div class="container-business">
        <div class="why">
            <h1>WHY MYWAY ?</h1>
{{--            <img src="{{asset('assets/image/dummy/benefit.png')}}" alt="">--}}
{{--            <span class="d-block">Simple description about why you must join myway membership</span>--}}
            {!! $data->why !!}
        </div>
        <div class="box">
            <h1>Benefit You Will Get ?</h1>
            <div class="row">

                @foreach($data->benefit as $res)
                <div class="item">
                    <div class="{{($loop->index % 2 == 0) ? 'left' : 'right'}} nopd">
                        <img src="{{$res->image_url}}" alt="{{$res->title}}">
                    </div>
                    <div class="{{($loop->index % 2 == 0) ? 'left' : 'right'}} nopd">
                        <h2>{{$res->title}}</h2>
                        <span>{{$res->description}}</span>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="interest-join">
            <h1>INTEREST TO JOIN ?</h1>
            <a href="{{$wa->data}}" class="btn btn-unggu btn-lg" role="button" aria-pressed="true">JOIN NOW</a>
        </div>
    </div>

@stop
