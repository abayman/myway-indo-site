@extends('master')
@section('title', 'Home')


@section('content')
    <div class="banner home">
{{--        <img src="{{asset('assets/image/dummy/promo_banner.png')}}" alt="">--}}
        <img src="{{asset('assets/image/dummy/promo.png')}}" alt="">
    </div>
    <div class="container-promo-detail">
        <div class="box">
            <h1 class="abu f40">{{$data->title}}</h1>
            <span class="date">Valid until: <strong>{{date('d F Y', strtotime($data->end_date))}}</strong></span>
            {!! $data->description !!}
            {{--            <p>--}}
{{--                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.--}}
{{--                <br><br>Term and condition:--}}
{{--            </p>--}}
{{--            <ul>--}}
{{--                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>--}}
{{--                <li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</li>--}}
{{--                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>--}}
{{--                <li>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</li>--}}
{{--            </ul>--}}
        </div>
    </div>

@stop
