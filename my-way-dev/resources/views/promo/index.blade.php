@extends('master')
@section('title', 'Home')


@section('content')
    <div class="banner about">
        <div class="desc">
            <h1 class="bold">PROMO</h1>
        </div>
        <img src="{{asset('assets/image/dummy/promo.png')}}" alt="">
    </div>
   <div class="container-promo">
       <div class="row">
           @if(count($data->data) > 0 )
               @foreach($data->data as $res)
                   <div class="col-4 mb90 box-promo">
                       <a href="{{route('promo.detail',['id' => $res->id,'slug' => \Str::slug($res->title)])}}">
                           <img src="{{$res->image_url}}" alt="">
                           <div class="desc">
                               {{$res->id.' '.$res->title}}
                               <span>Util {{date('d F Y', strtotime($res->end_date))}}</span>
                           </div>
                       </a>
                   </div>
               @endforeach
           @else
               <div class="col-12 mb90 box-promo">
                   <span class="text-center d-block">Tidak Ada Promo</span>
               </div>
           @endif
       </div>
       @if ($data->next_page_url != '')
           <div class="load-more">
               <hr class="pink left">
               <a href="{{route('promo.page',['page' => $page])}}" class="btn btn-lg" role="button" aria-pressed="true">Load More</a>
               <hr class="pink right">
           </div>
       @endif
   </div>

@stop
