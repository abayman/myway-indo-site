<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Voyager;
use Illuminate\Support\Arr;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'product';
    protected $dates = ['deleted_at'];
    protected $appends = ['rating'];

    public function getImagesAttribute()
    {
        $data = [];
        $value = json_decode($this->image);
        foreach ($value as $val){
            $data[] = Voyager::image($val);
        }
        return $data;
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'cate_id','id')
            ->with('child');

    }

    public function comments()
    {
        return $this->hasMany('App\Review','product_id','id');
    }

    public function getRatingAttribute($value)
    {
        $rating_data = [];
        $total_review =  count($this->comments);
        $total_rating = 0;
        $detail_rating= [];
        if (isset($this->comments)){
            foreach ($this->comments as $row){
                $total_rating= $total_rating + $row->rating;
                $rating_data[$row->rating][] = $row;
            }
        }
        for ($i=1;$i<=5;$i++){
            $total = (isset($rating_data[$i])) ? count($rating_data[$i]) : 0;
            $detail_rating[$i]=[
                'total' => $total,
                'nilai' => $this->HitungPersen($total_review,$total)
            ];
        }
        return $data = [
            'total_review' => $total_review,
            'rating' => (count($this->comments) > 0) ? $total_rating / $total_review : 0,
            'detail' => $detail_rating,
        ];
    }

    private function HitungPersen($nilaitotal,$nilai)
    {
        if ($nilai != 0 && $nilaitotal != 0){
            return ($nilai/$nilaitotal*100).'%';
        }
        return '0%';
    }
}
