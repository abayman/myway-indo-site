<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Voyager;

class Banner extends Model
{
    protected $table = 'banner';
    protected $casts =[
        'start_date' => 'datetime',
        'end_date' => 'datetime'
    ];
    protected $appends = ['image_url','image_mobile_url'];

    public function getImageUrlAttribute($value)
    {
        return Voyager::image($this->image);
    }


    public function getImageMobileUrlAttribute($value)
    {
        return Voyager::image($this->image_mobile);
    }

    public function scopeActive($query){
        $date = date('Y-m-d H:i:s');
        $query->whereRaw("timestamp(start_date) <= '$date' and timestamp(end_date) >= '$date'");
    }
}
