<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Voyager;

class Story extends Model
{
    protected $appends = array('image_url');

    public function getImageUrlAttribute($value)
    {
        return Voyager::image($this->image);
    }
}
