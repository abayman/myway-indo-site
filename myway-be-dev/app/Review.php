<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Review extends Model
{
    use SoftDeletes;
    protected $table = 'review';
    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->belongsTo('App\Product','id','product_id');
    }
}
