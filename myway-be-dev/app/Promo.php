<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Voyager;

class Promo extends Model
{
    use SoftDeletes;

    protected $table = 'promo';
    protected $dates = ['deleted_at'];

    protected $appends = array('image_url');

    public function getImageUrlAttribute($value)
    {
        return Voyager::image($this->image);
    }
}
