<?php


namespace App\Http\Controllers\API;

use App\Testimonial;
use Illuminate\Http\Request;

class TestiController
{
    public function show(Request $request)
    {
        $data = [];
        $limit = $request->input('limit');
        if ($limit){
            $q = Testimonial::take($limit)->get();
        }else{
            $q = Testimonial::all();
        }

        foreach ($q as  $val){
            $data[] = $val;
        }
        return response()->json(['data' => $data]);
    }

    public function detail(Request $request, $id)
    {
        $q = Testimonial::find($id);
        return response()->json(['data' => $q]);
    }
}
