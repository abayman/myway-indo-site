<?php


namespace App\Http\Controllers\API;


use App\Banner;

class BannerController
{
    public function show()
    {
        $data = Banner::active()
            ->orderBy('sort', 'asc')
            ->get();
        return response()->json(['data' => $data]);
    }
}
