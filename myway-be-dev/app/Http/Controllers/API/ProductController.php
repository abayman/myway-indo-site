<?php

namespace App\Http\Controllers\API;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ProductController extends Controller
{
    public function show(Request $request)
    {
        if ($request->input('type') == '1'){
            $q = Product::with('category')->where('is_rekomen','=',1)->get();
        }elseif ($request->input('type') == '2'){
            $q = Product::with('category')->where('is_new','=',1)->get();
        }else{
            $q = Product::with('category')->get();
        }

        $data = $this->CateArr($q);
        return response()->json(['data' => $data]);
    }

    public function cate()
    {
        $data = Category::display();
        return response()->json(['data' => $data]);
    }

    public function detail(Request $request, $id)
    {
        $data = Product::with('category')
            ->with('comments')
            ->find($id);
        $data['image'] = $data['images'];
        $data['cate_name'] = [
            'name' =>  isset($data['category']['child']['categories']['name']) ? $data['category']['child']['categories']['name'] : '' ,
            'slug' =>  isset($data['category']['child']['categories']['slug']) ? $data['category']['child']['categories']['name'] : '',
        ];
        $data['cate_sub'] = [
            'name' =>  isset($data['category']['child']['name']) ? $data['category']['child']['name'] : '' ,
            'slug' =>  isset($data['category']['child']['slug']) ? $data['category']['child']['name'] : '',
        ];
        $data['cate_child'] = [
            'name' =>  isset($data['category']['name']) ? $data['category']['name'] : '',
            'slug' =>  isset($data['category']['slug']) ? $data['category']['slug'] : '',
        ];

        return response()->json(['data' => $data]);
    }

    public function other($id)
    {
        $cate = Product::with('category')->find($id);
        $q = Product::with('category')
            ->where('id','!=', $id)
            ->where('cate_id',$cate->cate_id)
            ->inRandomOrder()
            ->take(8)
            ->get();
        $data = $this->CateArr($q);

        return response()->json(['data' => $data]);
    }

    public function ReviewAdd(Request $request)
    {
        $review = new Review();
        $review->name = $request->name;
        $review->rating = $request->rating;
        $review->comments = $request->comments;
        $review->product_id = $request->product_id;
        $q = $review->save();

        return response()->json(['status' => $q]);
    }

    public function ListByCate(Request $request, $cate)
    {
        $resp = [];
        $sort = $request->get('sort');
        if ($sort == 1){
            $q = Product::whereHas('category' , function ($query) use ($cate) {
                $query->where('slug','=', $cate);
            })
            ->orderBy('price','asc')
            ->paginate(12);
        }elseif ($sort==2){
            $q = Product::whereHas('category' , function ($query) use ($cate) {
                $query->where('slug','=', $cate);
            })
            ->orderBy('price','desc')
            ->paginate(12);
        }elseif ($sort==3){
            $q = Product::whereHas('category' , function ($query) use ($cate) {
                $query->where('slug','=', $cate);
            })
            ->withCount(['comments as average_rating' => function($query) {
                $query->select(DB::raw('coalesce(avg(rating),0)'));
            }])->orderBy('average_rating', 'desc')
            ->paginate(12);
        }else{
            $q = Product::whereHas('category' , function ($query) use ($cate) {
                $query->where('slug','=', $cate);
            })
            ->paginate(12);
        }

        $data = $this->CateArr($q);

        //Filter by slug
//        $i = 0;
//        foreach ($data as $row){
//            if ($cate == $row->cate_name['slug'] || $cate == $row->cate_sub['slug'] || $cate == $row->cate_child['slug']) {
//                if ($i < 12){php
//                    $resp[] = $row;
//                    $i++;
//                }
//            }
//        }

        return response()->json([
            'data' => $data,
            'last_page' => $q->lastPage()
        ]);
    }

    private function CateArr($q=[])
    {
        $data=[];
        foreach ($q as $key => $val){
            $data[$key] = $val;
            $data[$key]['image'] = $q[$key]['images'];
            $data[$key]['cate_name'] = [
                'name' =>  isset($q[$key]['category']['child']['categories']['name']) ? $q[$key]['category']['child']['categories']['name'] : '' ,
                'slug' =>  isset($q[$key]['category']['child']['categories']['slug']) ? $q[$key]['category']['child']['categories']['slug'] : '',
            ];
            $data[$key]['cate_sub'] = [
                'name' =>  isset($q[$key]['category']['child']['name']) ? $q[$key]['category']['child']['name'] : '' ,
                'slug' =>  isset($q[$key]['category']['child']['slug']) ? $q[$key]['category']['child']['slug'] : '',
            ];
            $data[$key]['cate_child'] = [
                'name' =>  $q[$key]['category']['name'],
                'slug' =>  $q[$key]['category']['slug'],
            ];
        }
        return $data;
    }

    public function search(Request $request)
    {
        $q = Product::where('name','like','%'.$request->q.'%')
            ->orWhere('code','like','%'.$request->q.'%')
            ->get();
        $data = $this->CateArr($q);

        return response()->json(['data' => $data]);

    }



}
