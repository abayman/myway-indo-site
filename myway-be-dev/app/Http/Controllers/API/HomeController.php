<?php


namespace App\Http\Controllers\API;


use App\About;
use App\Contact;

class HomeController
{
    public function footer()
    {
        $data =[
            'footer' =>menu('Footer','_json'),
            'fb' => setting('sosmed.sosmed_fb'),
            'twitter' => setting('sosmed.twitter'),
            'instagram' => setting('sosmed.instagram'),
            'youtube' => setting('sosmed.youtube'),
        ];
        return response()->json(['data' => $data]);
    }

    public function contact()
    {
        $data = Contact::all();
        return response()->json(['data' => $data]);
    }

    public function about()
    {
        $data = About::all();
        return response()->json(['data' => $data]);
    }

    public function MenuNav()
    {
        $data = menu('nav','_json');
        return response()->json(['data' => $data]);
    }
}
