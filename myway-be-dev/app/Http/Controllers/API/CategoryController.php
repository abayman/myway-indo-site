<?php


namespace App\Http\Controllers\API;


use App\Category;

class CategoryController
{
    public function show()
    {
        $data = Category::display();
        return response()->json(['data' => $data]);
    }
}
