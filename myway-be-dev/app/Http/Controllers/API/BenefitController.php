<?php


namespace App\Http\Controllers\API;


use App\Benefit;
use App\Testimonial;
use Illuminate\Http\Request;

class BenefitController
{
    public function show(Request $request)
    {
        $q = Benefit::all();
        $data = [
            'why' => setting('frontend.why'),
            'benefit' => $q
        ];
        return response()->json(['data' => $data]);
    }

    public function wa()
    {
        $data =  'https://wa.me/'.setting('order.wa').'?text='.urlencode(setting('order.text'));
        return response()->json(['data' => $data]);
    }
}
