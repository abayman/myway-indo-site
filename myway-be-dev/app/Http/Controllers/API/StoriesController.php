<?php


namespace App\Http\Controllers\API;


use App\Story;
use Illuminate\Http\Request;

class StoriesController
{
    public function show(Request $request)
    {
        $data = [];
        $limit = $request->input('limit');
        if ($limit){
            $q = Story::take($limit)->get();
        }else{
            $q = Story::all();
        }

        foreach ($q as  $val){
            $data[$val->category][] = $val;
        }
        return response()->json(['data' => $data]);
    }

    public function detail(Request $request, $id)
    {
        $q = Story::find($id);
        return response()->json(['data' => $q]);
    }

    public function except(Request $request, $id)
    {
        $limit = $request->input('limit');
        $q = Story::where('id','!=', $id)->inRandomOrder()->take($limit)->get();
        return response()->json(['data' => $q]);
    }
}
