<?php


namespace App\Http\Controllers\API;


use App\Promo;
use App\Testimonial;
use Illuminate\Http\Request;

class PromoController
{
    public function show(Request $request)
    {
        $q = Promo::where('end_date','>=', date('Y-m-d H:i:s'))->paginate(9);

        return response()->json(['data' => $q]);
    }

    public function detail(Request $request, $id)
    {
        $q = Promo::find($id);
        return response()->json(['data' => $q]);
    }
}
