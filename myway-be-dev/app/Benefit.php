<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Voyager;


class Benefit extends Model
{
    protected $table = 'benefit';

    protected $appends = array('image_url');

    public function getImageUrlAttribute($value)
    {
        return Voyager::image($this->image);
    }
}
