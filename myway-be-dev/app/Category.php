<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Events\MenuDisplay;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;

class Category extends Model
{
    use Translatable;

    protected $translatable = ['slug', 'name'];

    protected $table = 'categories';

    protected $fillable = ['slug', 'name'];


    public function parentId()
    {
        return $this->belongsTo(self::class);
    }

    public function parent_items()
    {
        return $this->hasMany(self::class,'id')
            ->whereNull('parent_id');
    }

    public function children()
    {
        return $this->hasMany(self::class, 'parent_id')
            ->with('children')
            ->orderBy('order');
    }

    public static function display()
    {
        $menu =  static::with(['children' => function ($q) {
                $q->orderBy('order');
            }])->whereNull('parent_id')->get();
        return $menu;

    }

    //Untuk di product API
    public function categories()
    {
        return $this->belongsTo(Category::class,'parent_id','id');
    }

    public function child()
    {
        return $this->belongsTo(Category::class,'parent_id','id')
            ->with('categories');
    }

}
