<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {

    Route::prefix('product')->group(function () {
        Route::get('get', 'API\ProductController@show')->name('product.get');
        Route::get('cate', 'API\ProductController@cate')->name('product.cate');
        Route::get('list-cate/{cate}', 'API\ProductController@ListByCate')->name('product.list');
        Route::get('detail/{id}', 'API\ProductController@detail')->name('product.detail');
        Route::get('other/{id}', 'API\ProductController@other')->name('product.other');
        Route::get('search', 'API\ProductController@search')->name('product.search');
    });
    Route::prefix('review')->group(function () {
        Route::post('add', 'API\ProductController@ReviewAdd')->name('review.add');
    });

    Route::prefix('promo')->group(function () {
        Route::get('get', 'API\PromoController@show')->name('promo.get');
        Route::get('detail/{id}', 'API\PromoController@detail')->name('promo.detail');
    });

    Route::prefix('stories')->group(function () {
        Route::get('get', 'API\StoriesController@show')->name('stories.get');
        Route::get('detail/{id}', 'API\StoriesController@detail')->name('stories.detail');
        Route::get('except/{id}', 'API\StoriesController@except')->name('stories.except');
    });

    Route::prefix('testi')->group(function () {
        Route::get('get', 'API\TestiController@show')->name('testi.get');
        Route::get('detail/{id}', 'API\TestiController@detail')->name('testi.detail');
    });

    Route::prefix('benefit')->group(function () {
        Route::get('get', 'API\BenefitController@show')->name('benefit.get');
        Route::get('wa', 'API\BenefitController@wa')->name('benefit.wa');
    });

    Route::prefix('page')->group(function () {
        Route::get('footer', 'API\HomeController@footer')->name('page.footer');
        Route::get('nav', 'API\HomeController@MenuNav')->name('page.nav');
        Route::get('contact', 'API\HomeController@contact')->name('page.contact');
        Route::get('about', 'API\HomeController@about')->name('page.about');
    });

    Route::prefix('category')->group(function () {
        Route::get('/', 'API\CategoryController@show')->name('category.list');
    });

    Route::prefix('banner')->group(function () {
        Route::get('/', 'API\BannerController@show')->name('banner.list');
    });
});
